package net.restapp.restcontroller;

import io.swagger.annotations.*;
import net.restapp.dto.EventCreateDTO;
import net.restapp.dto.EventReadDTO;
import net.restapp.exception.EntityNullException;
import net.restapp.mapper.DtoMapper;
import net.restapp.model.Event;
import net.restapp.servise.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/event", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
@Api(value="event", description="Operations pertaining to event")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Wrong arguments"),
        @ApiResponse(code = 401, message = "You are not authorized to do this action")
})

public class EventController {

    @Autowired
    IService<Event> eventService;

    @Autowired
    DtoMapper mapper;

//----------------------get -------------------------

    @ApiOperation(value = "View event by id", response = EventReadDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event successfully retrieved"),
            @ApiResponse(code = 403, message = "Accessing retrieving the event you were trying to reach is forbidden")
    })
    @GetMapping(value = "/{eventId}")
    public EventReadDTO getDepartment(
            @ApiParam(value = "id of the Event", required = true) @PathVariable("eventId") Long eventId) {

        Event event = eventService.getById(eventId);
        if (event == null) {
            String msg = String.format("There is no event with id: %d", eventId);
            throw new EntityNotFoundException(msg);
        }
        return mapper.simpleFieldMap(event,EventReadDTO.class);
    }

//-------------------------delete --------------------------------
    @ApiOperation(value = "Delete event by id", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event successfully deleted"),
            @ApiResponse(code = 403, message = "Accessing deleting the employee you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The event you were trying to reach is not found")
    })
    @DeleteMapping(value = "/{eventId}")
    public ResponseEntity deleteDepartment(
            @ApiParam(value = "id of the Event", required = true) @PathVariable("eventId") Long eventId) {

        Event event = eventService.getById(eventId);
        if (event == null) {
            String msg = String.format("There is no departments with id: %d", eventId);
            throw new EntityNotFoundException(msg);
        }
        eventService.delete(eventId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

//----------------------------edit -----------------------------------
    @ApiOperation(value = "Update event by id", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Event successfully updated"),
            @ApiResponse(code = 403, message = "Accessing updating the employee you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The event you were trying to reach is not found")
    })
    @PostMapping(value = "/{eventId}")
    public ResponseEntity editDepartment(@ApiParam(value = "id of the Event", required = true)  @PathVariable("eventId") Long eventId,
                                                 @ApiParam(value = "json body of the Event", required = true) @RequestBody @Valid EventCreateDTO dto) {

        Event event1 = eventService.getById(eventId);
        if (event1 == null) {
            String msg = String.format("There is no departments with id: %d", eventId);
            throw new EntityNotFoundException(msg);
        }
        if (dto == null) {
            throw new EntityNullException("event can't be null");
        }
        Event event = mapper.map(dto,Event.class);
        event.setId(eventId);
        eventService.save(event);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
//-----------------------------add  ---------------------------
    @ApiOperation(value = "Save event to database", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event successfully created"),
            @ApiResponse(code = 403, message = "Accessing creating the employee you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The event you were trying to reach is not found")
    })
    @PostMapping(value = "/add")
    public ResponseEntity saveDepartment(
            @ApiParam(value = "json body of the Event", required = true) @RequestBody @Valid EventCreateDTO dto) {

        if (dto == null) {
            throw new EntityNullException("event can't be null");
        }
        Event event = mapper.map(dto,Event.class);
        eventService.save(event);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

//-----------------------------getAll ---------------------------
    @ApiOperation(value = "Retrieve all events", response = EventReadDTO.class, responseContainer="List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Events successfully retrieved"),
            @ApiResponse(code = 403, message = "Accessing retrieving events you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The is no events to retrieve")
    })
    @GetMapping(value = "/getAll")
    public List<EventReadDTO> getAllDepartment() {
        List<Event> listEvent = eventService.getAll();
        if (listEvent.isEmpty()) {
            String msg = "There is no events ";
            throw new EntityNotFoundException(msg);
        }
        return  mapper.listSimpleFieldMap(listEvent,EventReadDTO.class);
    }


}
