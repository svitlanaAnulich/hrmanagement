package net.restapp.restcontroller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.restapp.dto.UserReadDTO;
import net.restapp.dto.UserUpdateEmailDTO;
import net.restapp.dto.UserUpdatePasswordDTO;
import net.restapp.mapper.DtoMapper;
import net.restapp.model.User;
import net.restapp.servise.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/employees/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR", "ROLE_USER"})
@Api(value = "user", description = "Operations pertaining with login user")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Wrong arguments"),
        @ApiResponse(code = 401, message = "You are not authorized to do this action")
})
public class UserController {

    @Autowired
    private DtoMapper mapper;

    @Autowired
    UserService userService;

//------------------------------ get -----------------------------------
    @ApiOperation(value = "View a login user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Login user successfully showed"),
            @ApiResponse(code = 403, message = "Accessing updating the user you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The user is not found")
    })
    @GetMapping(value = "")
    public UserReadDTO getUser(HttpServletRequest request) {
        Long userId = getLoginUserId(request);
        User user = userService.getById(userId);
        return mapper.simpleFieldMap(user, UserReadDTO.class);
    }
//--------------------------------change password--------------------------------------------------
    @ApiOperation(value = "Update password for login User")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Password successfully updated"),
            @ApiResponse(code = 403, message = "Accessing updating the user you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The user you were trying to reach is not found")
    })
    @PostMapping(value = "/pass")
    public void setUser(@Valid @RequestBody UserUpdatePasswordDTO dto,
                                         HttpServletRequest request) {
        Long userId = getLoginUserId(request);
        userService.updateUserPasswordById(userId,dto);
    }

//--------------------------------change email--------------------------------------------------

    @ApiOperation(value = "Update email for login User")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Email successfully updated"),
            @ApiResponse(code = 403, message = "Accessing updating the user you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The user you were trying to reach is not found")
    })
    @PostMapping(value = "/email")
    public void setUser(@Valid @RequestBody UserUpdateEmailDTO dto, HttpServletRequest request) {
        Long userId = getLoginUserId(request);
        userService.updateUserEmailById(userId,dto);
    }

//-----------------------------------------------------------------------
    private Long getLoginUserId(HttpServletRequest request){
        String userLoginEmail=request.getUserPrincipal().getName();
        User user = userService.findByEmail(userLoginEmail);
        return user.getId();
    }

}
