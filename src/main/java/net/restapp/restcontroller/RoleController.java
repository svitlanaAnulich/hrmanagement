package net.restapp.restcontroller;

import io.swagger.annotations.*;
import net.restapp.dto.EmployeeChangeRoleDTO;
import net.restapp.dto.EmployeeReadDTO;
import net.restapp.mapper.DtoMapper;
import net.restapp.model.Employees;
import net.restapp.servise.EmployeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/employees/role", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(value = "employee", description = "Operations pertaining to role of employee")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Role successfully got"),
        @ApiResponse(code = 401, message = "You are not authorized to do this action"),
        @ApiResponse(code = 403, message = "Accessing updating the role you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The role you were trying to reach is not found"),
        @ApiResponse(code = 400, message = "Wrong arguments")
})
@Secured("ROLE_ADMIN")
public class RoleController {

    @Autowired
    EmployeesService employeesService;

    @Autowired
    private DtoMapper mapper;

//-------------------------------getAll with role ------------------------------------------------

    @ApiOperation(value = "View all employees by role ID", response = EmployeeReadDTO.class, responseContainer = "List")
    @GetMapping(value = "/{roleId}")
    public List<EmployeeReadDTO> getAllEmployees(@PathVariable("roleId") Long roleId) {

        List<Employees> employees = employeesService.getAllByRoleId(roleId);
        if (employees.isEmpty()) {
            throw new EntityNotFoundException();
        }
        return mapper.listSimpleFieldMap(employees, EmployeeReadDTO.class);

    }

//--------------------------------change role --------------------------------------------------------------
    @ApiOperation(value = "View all employees by role ID", response = Employees.class, responseContainer = "List")
    @PostMapping(value = "/")
    public void changeRoleForEmployee(@RequestBody @Valid EmployeeChangeRoleDTO dto) throws Exception {
         employeesService.updateEmployeeRole(dto);
    }

}
