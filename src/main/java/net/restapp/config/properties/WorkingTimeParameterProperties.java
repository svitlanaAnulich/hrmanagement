package net.restapp.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Configuration
@PropertySource("classpath:payment-calculate.properties")
@ConfigurationProperties(prefix = "working.time.parameters")
@Data
@NoArgsConstructor
public class WorkingTimeParameterProperties {

    @Min(value = 0, message = "workHoursPerDay should not be greater than 0")
    @Max(value = 1, message = "workHoursPerDay should not be less than 12")
    private int averageWorkHoursPerDay;

    @Min(value = 15, message = "averageWorkingDayPerMonth should not be greater than 15")
    @Max(value = 30, message = "averageWorkingDayPerMonth should not be less than 30")
    private int averageWorkingDayPerMonth;

    @Min(value = 0, message = "countMonthForCalculateSickPayment should not be greater than 0")
    @Max(value = 12, message = "countMonthForCalculateSickPayment should not be less than 12")
    private int countMonthForCalculateSickPayment;

    @Min(value = 0, message = "countMonthForCalculateSickPayment should not be greater than 0")
    @Max(value = 12, message = "countMonthForCalculateSickPayment should not be less than 12")
    private int countMonthForCalculateVacationPayment;

    @Min(value = 10, message = "countMonthForCalculateSickPayment should not be greater than 10")
    @Max(value = 20, message = "countMonthForCalculateSickPayment should not be less than 20")
    private BigDecimal maxWorkHourForDay;
}
