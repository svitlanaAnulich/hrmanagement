package net.restapp.config.properties;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Configuration
@PropertySource("classpath:show.properties")
@ConfigurationProperties(prefix = "working.hours")
@Data
@NoArgsConstructor
public class ShowProperties {

    @Min(value = 1, message = "workingHoursShowMaxDaysInFuture should not be greater than 1")
    @Max(value = 20, message = "workingHoursShowMaxDaysInFuture should not be less than 20")
    private int workingHoursShowMaxDaysInFuture;

    @Min(value = 1, message = "toThreeYear should not be greater than 1")
    @Max(value = 3, message = "toThreeYear should not be less than 3")
    private int workingHoursShowMaxDaysInPast;
}
