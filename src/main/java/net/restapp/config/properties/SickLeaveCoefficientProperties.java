package net.restapp.config.properties;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Configuration
@PropertySource("classpath:payment-calculate.properties")
@ConfigurationProperties(prefix = "sick.coefficient")
@Data
@NoArgsConstructor
public class SickLeaveCoefficientProperties {

    @Min(value = 0, message = "toThreeYear should not be greater than 0")
    @Max(value = 1, message = "toThreeYear should not be less than 1")
    private BigDecimal toThreeYear;

    @Min(value = 0, message = "toFiveYear should not be greater than 0")
    @Max(value = 1, message = "toFiveYear should not be less than 1")
    private BigDecimal toFiveYear;

    @Min(value = 0, message = "toEightYear should not be greater than 0")
    @Max(value = 1, message = "toEightYear should not be less than 1")
    private BigDecimal toEightYear;

    @Min(value = 0, message = "fromEightYear should not be greater than 0")
    @Max(value = 1, message = "fromEightYear should not be less than 1")
    private BigDecimal fromEightYear;

}
